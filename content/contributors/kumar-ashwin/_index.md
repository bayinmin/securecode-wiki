---
title: Kumar Ashwin
image: 
    url: ./kumar-ashwin.jpg
    alt: Kumar Ashwin's Profile Picture
website: https://0xCardinal.com
socials:
    linkedin: https://www.linkedin.com/in/0xCardinal/
    twitter: https://twitter.com/0xCardinal
    github: https://github.com/krAshwin
draft: false
type: contributors
layout: single
---
Ashwin is a Security Consultant at Payatu, dealing in web security assessments and learning about cloud security on the go. He actively contributes to security communities like null. He often writes about his experience on his [blog](https://krash.dev) and he is surely the social being you will find at a snooker table dominated by a geeky conversation!
<!-- Your description here! -->
