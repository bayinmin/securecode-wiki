---
title: Amit kumar
image: 
    url: ./amit_kumar.jpg
    alt: amit_kumar
website: https://effortlesssecurity.in/
socials:
    linkedin: https://www.linkedin.com/in/amit-parjapat/
    twitter: https://twitter.com/bit3threat
    github: https://github.com/effortlessdevsec
draft: false # Change it to false if you want it published.
# Do not change the below values.
type: contributors
layout: single
# Note: Always use https:// whenever putting up links. For e.g., https://payatu.com
# All the fields above are optional
---
Amit Kumar is an experienced Security Consultant in Payatu. Experienced in  Web Application,Think Client Application, Mobile Application & Web Services penetration testing.He is also expert in languages like Python, Bash, PHP, Java,javascript.
<!-- Your description here! -->
