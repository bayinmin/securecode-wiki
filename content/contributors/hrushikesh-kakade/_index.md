---
title: Hrushikesh Kakade
image: 
    url: https://payatu.com/static/images/members/HRUSHIKESH.png
    alt: https://payatu.com/static/images/members/HRUSHIKESH.png
website: https://hkh4cks.com
socials:
    linkedin: https://www.linkedin.com/in/hrushikeshkakade/
    twitter: https://twitter.com/hkh4cks
    github: https://github.com/hrushikeshk
draft: false # Change it to false if you want it published.
# Do not change the below values.
type: contributors
layout: single
# Note: Always use https:// whenever putting up links. For e.g., https://payatu.com
# All the fields above are optional
---
Hrushikesh is a Red Team Researcher at Payatu. He specializes in advanced assessments of Mobile Security (Android and iOS), Network Infrastructure Security, DevSecOps, Container security, Web security, and Cloud security. He is an active member of local Cybersecurity chapters and also a trainer at Nullcon. He is an Open Source Contributor and has a keen understanding of Linux Internals.
<!-- Your description here! -->
