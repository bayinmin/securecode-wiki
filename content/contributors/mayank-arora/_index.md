---
title: Mayank Arora
image: 
    url: https://i.ibb.co/ThSQvvW/mayank-website-photo.jpg
    alt: https://i.ibb.co/ThSQvvW/mayank-website-photo.jpg
# website: https://thej0lt.com/
socials:
    linkedin: https://www.linkedin.com/in/connect2mayank
    twitter: https://twitter.com/mayankarora1719
    github: https://github.com/Cyber0ps
draft: false # Change it to false if you want it published.
# Do not change the below values.
type: contributors
layout: single
# Note: Always use https:// whenever putting up links. For e.g., https://payatu.com
# All the fields above are optional
---
Mayank is an experienced Associate Security Consultant in Payatu. He specializes in assessments of Mobile Security ,  Web Application security, Network Infrastructure Security, DevSecOps and Cloud security. He is also expert in languages like Python , C and Javascript.
<!-- Your description here! -->
