---
title: Manmeet Singh
image: 
    url: https://i.ibb.co/sRcMthV/7635.jpg
    alt: https://i.ibb.co/sRcMthV/7635.jpg
website: https://thej0lt.com/
socials:
    linkedin: https://www.linkedin.com/in/j0lt
    twitter: https://twitter.com/_j0lt
    github: https://github.com/j0lt-github
draft: false # Change it to false if you want it published.
# Do not change the below values.
type: contributors
layout: single
# Note: Always use https:// whenever putting up links. For e.g., https://payatu.com
# All the fields above are optional
---
Manmeet aka j0lt is an experienced Security Consultant in Payatu. Experienced in pentesting Web Application, Mobile Application, Thick and Thin Client application, Enterprise Network and Cloud environment.He is also expert in languages like Python , C and Javascript.
<!-- Your description here! -->
